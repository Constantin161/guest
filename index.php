<?php
use Guest\Guestbook;

include('function.php');
include('config.php');
include('Guestbook.php');
include('vendor/autoload.php');

$guestbook = new Guestbook(DB, LOGIN, PW, 'guestbook');

$list = $guestbook->last();

$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

$template = $twig->loadTemplate('list.tpl');
$params = array(
	'list' => $list,
	);
$template->display($params);