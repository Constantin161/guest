<!doctype HTML>
<html>
<head>
	<title>Создать запись</title>
	<meta charset = 'UTF-8'>
	<link rel="stylesheet" href="templates/css/main.css">
</head>
<body>
	<h2>Создать запись</h2>
	<form method = "post">
		Ваше имя<br>
		<input type="text" name="name" placeholder="Введите ваше имя" value="{{ name }}"/><br>
		Сообщение<br>
		<textarea name="message" placeholder="Введите текст сообщения">{{ message }}</textarea><br>
		<input type="submit" value="Создать запись" />
	</form>
	<div class="error"> {{ error_text }} </div>
</body>