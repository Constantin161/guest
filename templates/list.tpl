<!doctype HTML>
<html>
<head>
	<title>Список записей</title>
	<meta charset = 'UTF-8'>
	<link rel="stylesheet" href="templates/css/main.css">
</head>
<body>
		<h2>Список записей</h2>
		<form class="adder" action="create.php" method="post">
			<input type="submit" value="Добавить запись" />
		</form>
	<table>
		<tr>
			<th>Автор</th>
			<th>Статья</th>
			<th>Дата создания</th>
		<tr>	
		{%	for post in list %}
			<tr>
				<td>{{ post.name }}</td>
				<td>{{ post.message|nl2br }}</td>
				<td>{{ post.crdate|date("d.m.Y H:i:s") }}</td>
			</tr>
		{% endfor %}
	</table>
</body>