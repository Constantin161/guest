<?php
use Guest\Guestbook;

error_reporting(E_ALL);

include('function.php');
include('config.php');
include('Guestbook.php');
include('vendor/autoload.php');

$error_text = null;
$name = isset($_POST['name']) ? clearStr($_POST['name']) : null;
$message = isset($_POST['message']) ? clearStr($_POST['message']) : null;


if(isset($_POST['name'])) {
    if(empty($name) || empty($message)) {
        $error_text = "Вы заполнили не все поля!";
    }
    else {
        $new[] = $name;
        $new[] = $message;
        $guestbook = new Guestbook(DB, LOGIN, PW, 'guestbook');
        $guestbook->create($new);
        header('Location: index.php');
	}
}


$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

$template = $twig->loadTemplate('form.tpl');
$params = array(
	'error_text' => $error_text,
	'name' => $name,
	'message' => $message
	);
$template->display($params);