<?php
// функция очищает строку от муссра
function clearStr($str) {
    $str=strip_tags($str);
    $str=stripslashes($str);
    $str=htmlspecialchars($str);
    $str=trim($str);
    return $str;
}