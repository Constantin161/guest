<?php

namespace Guest;

use PDO;

class Guestbook 
{
    private $pdo;

    private $table_name;

    public function __construct($dbname, $login, $password, $table_name) {
        $this->table_name = $table_name;
        $this->pdo = new PDO($dbname, $login, $password);
        $this->pdo->exec("SET NAMES utf8;");
    }

    public function last() {
        $sql = "SELECT * FROM ".$this->table_name." ORDER BY crdate DESC LIMIT 20";
        $stm = $this->pdo->prepare($sql);
        $stm->execute();
        return $stm->fetchAll();
    }

    public function create($new) {
        $sql = "INSERT INTO ".$this->table_name." (crdate, name, message)
                VALUES  (NOW(), ?, ?)";
        $stm = $this->pdo->prepare($sql);
        $stm->execute($new);
		return $this->pdo->lastInsertId();
    }
}
